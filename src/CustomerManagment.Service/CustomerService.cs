﻿using CustomerManagment.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomerManagment.Domain;

namespace CustomerManagment.Service
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public async Task CreateCustomer(Customer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException("customer");
            }

            _customerRepository.Add(customer);
            var result =  await _customerRepository.SaveChangesAsync();
        }

        public async Task DeleteCustomer(int customerId)
        {
            var customer = await _customerRepository.GetAsync(customerId);

            if (customer != null)
            {
                _customerRepository.Remove(customer);
                var result = await _customerRepository.SaveChangesAsync();
            } 
        }

        public IEnumerable<Customer> GetAllCustomers()
        {
            return _customerRepository.GetAll();
        }

        public async Task<Customer> GetCustomer(int customerId)
        {
            return await _customerRepository.GetAsync(customerId);
        }

        public async Task UpdateCustomer(Customer customer)
        {
            if(customer != null)
            {
                _customerRepository.Update(customer);
                var result = await _customerRepository.SaveChangesAsync();
            }
        }
    }
}
