﻿using CustomerManagment.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerManagment.Service
{
    public interface ICustomerService : IService
    {
        Task<Customer> GetCustomer(int customerId);
        IEnumerable<Customer> GetAllCustomers();
        Task CreateCustomer(Customer customer);
        Task UpdateCustomer(Customer customer);
        Task DeleteCustomer(int customerId);
    }
}
