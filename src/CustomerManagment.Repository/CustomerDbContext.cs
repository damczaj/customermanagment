﻿using CustomerManagment.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerManagment.Repository
{
    public class CustomerDbContext : DbContext
    {
        public CustomerDbContext() : base("CustomerContext")
        {
        }

        public CustomerDbContext(string nameOrConnectioString) : base(nameOrConnectioString)
        {

        }

        public DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

    }
}
