﻿using CustomerManagment.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerManagment.Repository
{
    public interface ICustomerRepository : IRepository
    {
        Task<Customer> GetAsync(int id);
        IEnumerable<Customer> GetAll();
        void Add(Customer customer);
        void Update(Customer customer);
        void Remove(Customer custome);
        Task<int> SaveChangesAsync();
    }
}
