﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomerManagment.Domain;
using System.Data.Entity;

namespace CustomerManagment.Repository
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly CustomerDbContext _dbContext;
        
        public CustomerRepository(CustomerDbContext dbContext)
        {
            _dbContext = dbContext;
            
        }

        public void Add(Customer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException("customer");
            }
            _dbContext.Customers.Add(customer);
        }

        public IEnumerable<Customer> GetAll()
        {
            return _dbContext.Customers.AsEnumerable();
        }

        public async Task<Customer> GetAsync(int id)
        {
            return await _dbContext.Customers.FindAsync(id);
            
        }

        public void Remove(Customer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException("customer");
            }
            _dbContext.Customers.Remove(customer);
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _dbContext.SaveChangesAsync();
        }

        public void Update(Customer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException("customer");
            }
            _dbContext.Customers.Attach(customer);
            _dbContext.Entry(customer).State = EntityState.Modified;
        }

       
    }
}
