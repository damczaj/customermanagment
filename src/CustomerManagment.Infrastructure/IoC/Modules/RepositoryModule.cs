﻿using Autofac;
using CustomerManagment.Infrastructure.Settings;
using CustomerManagment.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerManagment.Infrastructure.IoC.Modules
{
    public class RepositoryModule : Module
    {
        private readonly IConfiguration _configuration;
        public RepositoryModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new CustomerDbContext(_configuration.ConnectionString));
            builder.RegisterType<CustomerRepository>().As<ICustomerRepository>().InstancePerRequest();
        }
    }

}
