﻿using Autofac;
using CustomerManagment.Infrastructure.IoC.Modules;
using CustomerManagment.Infrastructure.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerManagment.Infrastructure.IoC
{
    public class ContainerModule : Autofac.Module
    {
        private readonly IConfiguration _configuration;
        public ContainerModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new RepositoryModule(_configuration));
            builder.RegisterModule<ServiceModule>();
        }
    }
}
