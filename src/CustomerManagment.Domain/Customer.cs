﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerManagment.Domain
{
    public class Customer : BaseEntity
    {
        [Key]
        public int CustomerId { get; set; }

        [Required(ErrorMessage = "Your must provide a Name")]
        [Display(Name = "Name")]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Your must provide a Surname")]
        [Display(Name = "Surname")]
        [DataType(DataType.Text)]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Your must provide a PhoneNumber")]
        [Display(Name = "Home Phone")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Your must provide a Street Number")]
        [Display(Name = "Street Number")]
        [DataType(DataType.Text)]
        public string StreetNumber { get; set; }

        [Required(ErrorMessage = "Your must provide a Street Name")]
        [Display(Name = "Street Name")]
        [DataType(DataType.Text)]
        public string StreetName { get; set; }

        [Required(ErrorMessage = "Your must provide a City")]
        [Display(Name = "City")]
        [DataType(DataType.Text)]
        public string City { get; set; }

        [Required(ErrorMessage = "Your must provide a Postal Code")]
        [Display(Name = "Postal Code")]
        [DataType(DataType.PostalCode)]
        public string PostalCode { get; set; }

    }
}
