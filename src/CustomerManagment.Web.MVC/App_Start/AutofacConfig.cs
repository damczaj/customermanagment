﻿using Autofac;
using Autofac.Integration.Mvc;
using CustomerManagment.Infrastructure.IoC;
using CustomerManagment.Infrastructure.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CustomerManagment.Web.MVC.App_Start
{
    public class AutofacConfig
    {
        public static void InitializeAutofac()
        {
            var builder = new ContainerBuilder();

            IConfiguration configuration = new Configuration();
            configuration.ConnectionString = 
                System.Configuration.ConfigurationManager.ConnectionStrings["CustomerContext"].ConnectionString;

            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterModule(new ContainerModule(configuration));

            var container = builder.Build();
            
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
        
    }
}