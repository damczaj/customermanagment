﻿using CustomerManagment.Domain;
using CustomerManagment.Service;
using CustomerManagment.Web.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CustomerManagment.Web.MVC.Controllers
{
    public class CustomerController : Controller
    {
        private readonly ICustomerService _customerService;
        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var customers = _customerService.GetAllCustomers();

            return View(customers);
        }



        [HttpGet]
        public ActionResult AddCustomer()
        {
            Customer model = new Customer();

            return PartialView("_AddCustomer", model);
        }

        [HttpPost]
        public async Task<ActionResult> AddCustomer(Customer model)
        {
            if(ModelState.IsValid)
            {
                await _customerService.CreateCustomer(model);
                
                if(model.CustomerId > 0)
                {
                    return RedirectToAction("Index", "Customer");
                }
            }
           
            return View("_AddCustomer",model);
            

        }

        [HttpGet]
        public async Task<ActionResult> EditCustomer(int? id)
        {
            Customer model = new Customer();

            if (id.HasValue && id != 0)
            {
                 model = await _customerService.GetCustomer(id.Value);
            }

            return PartialView("_EditCustomer", model);
        }

        [HttpPost]
        public async Task<ActionResult> EditCustomer(Customer model)
        {
            if (ModelState.IsValid)
            {
                await _customerService.UpdateCustomer(model);

                if (model.CustomerId > 0)
                {
                    return RedirectToAction("Index", "Customer");
                }
            }

            return View("_EditCustomer", model);
        }

        [HttpGet]
        public async Task<ActionResult> DeleteCustomer(int? id)
        {
            Customer model = new Customer();

            if (id.HasValue && id != 0)
            {
                model = await _customerService.GetCustomer(id.Value);
            }
            var customer = new DeleteCustomerViewModel()
            {
                FullName = $"{model.Name} {model.Surname}",
                Id = id
            };
        
            return PartialView("_DeleteCustomer", customer);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteCustomer(DeleteCustomerViewModel model)
        {
            if (ModelState.IsValid)
            {
                if(model.Id != 0 && model.Id.HasValue)
                {
                    await _customerService.DeleteCustomer(model.Id.Value);
                }
            }

            return RedirectToAction("Index", "Customer");

        }




    }
}
