﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerManagment.Web.MVC.Models
{
    public class DeleteCustomerViewModel
    {
        public string FullName { get; set; }
        public int? Id { get; set; }
    }
}